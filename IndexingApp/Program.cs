﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace IndexingApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var root = new FolderNode("D:\\books");
           
            using (var sw = new StreamWriter("D:\\fileToSend.txt"))
            {
                var stack = new Stack<FolderNode>();
                stack.Push(root);
                sw.WriteLine("Computer Name: " + Environment.MachineName);
                sw.WriteLine("IP address: " + Dns.GetHostAddresses(Environment.MachineName)[0].ToString());
                sw.WriteLine(root.FullPath + " " + root.Hash);
                while (stack.Count != 0)
                {
                    var element = stack.Pop();
                    foreach (var child in element.children)
                    {
                        if (child.Value is FolderNode) stack.Push((FolderNode)child.Value);
                        sw.WriteLine(child.Value.FullPath + " " + child.Value.Hash);
                    }
                }
            }
        }
    }
}
