﻿using System.Collections.Generic;
using System;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace IndexingApp
{
    [Serializable]
    public abstract class TreeNode
    {
        public string FullPath { get; set; } //includes the node name

        public string Hash { get; protected set; }

        public FolderNode Parent { get; set; }

        public TreeNode(string fullPath, FolderNode parent = null)
        {
            Parent = parent;
            FullPath = fullPath;
        }

        public abstract void ComputeHashCode();

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return Hash.Equals(((FolderNode)obj).Hash);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return Hash.GetHashCode();
        }

    }

    [Serializable]
    public class FolderNode : TreeNode
    {
        public readonly Dictionary<string, TreeNode> children = new Dictionary<string, TreeNode>();

        public FolderNode(string fullPath, FolderNode parent = null) : base(fullPath)
        {
            ComputeHashCode();
        }

        public void AddChild(TreeNode child)
        {
            children[child.FullPath] = child;
        }

        public override void ComputeHashCode()
        {
            var directory = new DirectoryInfo(FullPath);
            FileInfo[] files = null;
            DirectoryInfo[] subDirs = null;
            try
            {
                files = directory.GetFiles("*");
                subDirs = directory.GetDirectories();
            }
            // This is thrown if even one of the files requires permissions greater
            // than the application provides.
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }

            if (files != null)
                foreach (FileInfo file in files)
                {
                    var nodeFile = new FileNode(file.FullName, this);
                    AddChild(nodeFile);
                }

            if (subDirs != null)
                foreach (DirectoryInfo dirInfo in subDirs)
                {
                    var subFolder = new FolderNode(dirInfo.FullName, this);
                    AddChild(subFolder);
                }

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, this);
                memoryStream.Seek(0, SeekOrigin.Begin);
                Hash = SHA256.Create().ComputeHash(memoryStream)
                    .Select(b => b.ToString("x2"))
                    .Aggregate("", (a, c) => a + c);
            }
        }

    }

    [Serializable]
    class FileNode : TreeNode
    {
        public FileNode(string fullPath, FolderNode parent) : base(fullPath)
        {
            ComputeHashCode();
        }

        public void AddTo(FolderNode folder)
        {
            Parent = folder;
            try
            {
                folder.AddChild(this);
            }
            catch
            {
                Parent = null;
            }
        }

        public override void ComputeHashCode()
        {
            using (FileStream stream = System.IO.File.OpenRead(FullPath))
            {
                Hash = SHA256.Create().ComputeHash(stream)
                    .Select(b => b.ToString("x2"))
                    .Aggregate("", (a, c) => a + c);
            }
        }
    }

}
